'use strict';

var zxcvbn = function() {};

var is_loaded = false;
var is_loading = false;
var $loader;


/**
 * Load the external zxcvb library and update all passwordstrength elements on
 * the page.
 */
function loadStrengthCalculator() {
    is_loading = true;
    $loader = jQuery.ajax({
        url: '/js/zxcvbn.js',
        dataType: 'script'
    });


    $loader.done(function() {
        is_loading = false;
        is_loaded = true;
    });
}


jQuery.directive('js-passwordstrength', function($elt, attrs) {
    if (!is_loaded && !is_loading) {
        loadStrengthCalculator();
    }

    var $password = $elt.prevAll(':input');
    var defaults = 'Very weak,Weak,Moderate,Strong,Very strong';
    var config = attrs['js-passwordstrength'];
    var score_strings = (config ?
            config :
            defaults).split(',');

    function updatePasswordStrength() {
        var value = $password.val();
        if (value) {
            var score = zxcvbn(value).score;
            $elt.show().text(score_strings[score]);
            $elt.attr('data-strength', score);
        } else {
            $elt.hide();
        }
    }

    $password.on('keyup', updatePasswordStrength);
    $loader.done(updatePasswordStrength);
});
