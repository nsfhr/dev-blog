(function($) {
    var $target = $('[js-packagist-list]');

    var request = $.getJSON('https://packagist.org/search.json?q=symbid&callback=?');
    request.done(render);

    function render(data) {
        var html = '<h4>Symbid on Packagist:</h4>';
        html += '<ul class="list-unstyled blog-icon-list">';

        $.each(data.results, function(index, result) {
            html += '<li>';
            html += '<i class="fa fa-fw fa-cubes"></i>';
            html += '<a href="' + result.url + '">' + result.name + '</a>';

            html += '<div class="pull-right">';
                    html += '<i class="fa fa-fw fa-download"></i>' + result.downloads;
                    html += '<i class="fa fa-fw fa-ellipsis-v"></i>';
                    html += '<i class="fa fa-fw fa-star"></i>' + result.favers;
            html += '</div>';

            html += '<p>' + result.description + '</p>';
            html += '</li>';
        });

        html += '</ul><hr>';

        $target.append(html);
    }

})(jQuery);
