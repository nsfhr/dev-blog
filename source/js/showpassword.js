jQuery.directive('js-showpassword', function($elt) {
    var $password = $elt.prevAll(':input');
    var $checkbox = $elt.find(':checkbox');

    function toggle() {
        var type = $checkbox[0].checked ? 'text' : 'password';
        $password.attr('type', type);
    }

    $checkbox.on('change', toggle);
    toggle();
});
