---
title: The User Experience of creating a new password
tags:
    - ux
    - accounts
    - frontend
authors:
    - mennovanslooten
---

If you want people to sign up for your service, creating an account needs to be as painless as possible. One of the most
crucial steps in any registration form is creating a password. In this blog post we will look at two enhancements we
added to the standard password input to improve the user experience in our upcoming registration forms.

## Enhancement 1: preventing mistyped passwords

In 2009, [Jakob Nielsen](https://en.wikipedia.org/wiki/Jakob_Nielsen_\(usability_consultant\)) wrote an article called
[Stop Password Masking](http://www.nngroup.com/articles/stop-password-masking/). In it, he argued against the use of
bullets inside password fields to mask the password the user is typing. According to Nielsen, it does more harm than
good, causing users to mistype passwords which in turn triggers them to simplify or copy/paste passwords.

Unfortunately, In 2015, not much has changed. Password masking is still the default and many signup forms still sport
a *repeat password* field, to verify the user did not mistype his new password. Some sites, like Twitter and
GitHub, ask for an email address on signup and rely on the *forgot password* feature in stead.

For our signup forms we decided on a single masked password field, with a toggle to unmask the password. Powering this
feature is some simple jQuery-based code which is bound by
[jquery.directive.js](https://github.com/mennovanslooten/jquery.directive.js) to any element with a
`js-showpassword` attribute:

```html
<input type="password">
<label js-showpassword>
    <input type="checkbox"> Show password
</label>
```

```javascript
jQuery.directive('js-showpassword', function($elt) {
    var $password = $elt.prevAll(':input');
    var $checkbox = $elt.find(':checkbox');

    function toggle() {
        var type = $checkbox[0].checked ? 'text' : 'password';
        $password.attr('type', type);
    }

    $checkbox.on('change', toggle);
    toggle();
});
```

<div class="well blog-well">
    <input type="password" placeholder="Enter a password">
    <label js-showpassword>
        <input type="checkbox"> Show password
    </label>
</div>

## Enhancement 2: password strength meter

Naturally, we want our users to protect themselves with strong passwords. Unfortunately, there is a lot of
misinformation about what constitutes a strong password. Consequently, many sites enfore seemingly arbitrary rules on
password format, forcing the user to create passwords that look secure but are, ironically, easier to crack.
Requirements that passwords contain a mixture of special characters, numbers, capitals, etc. usually end up frustrating
users and makes passwords hard to remember.

For this reason, we decided to only enforce a minimum password length of 6 characters, and nothing in terms of
format. Instead we provide real-time feedback from a password strength meter to motivate users to create stronger
passwords. [Dropbox's zxcvbn](https://github.com/dropbox/zxcvbn) is a great stand-alone JavaScript library that
calculates a password's strength based on a large list of common passwords, patterns and entropy formulas.

Using jQuery and jquery.directive.js the zxcvbn calculation is bound to the input's `onkeyup` event.



```html
<input type="password">
<span js-passwordstrength></span>
```

```javascript
jQuery.directive('js-passwordstrength', function($elt, attrs) {
    var $password = $elt.prevAll(':input');
    var score_strings = ['Very weak', 'Weak', 'Moderate', 'Strong', 'Very strong'];

    function updatePasswordStrength() {
        var value = $password.val();
        if (value) {
            var score = zxcvbn(value).score;
            $elt.show().text(score_strings[score]);
        } else {
            $elt.hide();
        }
    }

    $password.on('keyup', updatePasswordStrength);
});
```

<div class="well blog-well">
    <input type="password" placeholder="Enter a password">
    <span class="label label-success" js-passwordstrength></span>
</div>


## Combining both features

Because the behavior is automatically bound by jquery.directive.com, combining the two features is only a matter of combining
the HTML elements.

```html
<input type="password">
<span js-passwordstrength></span>
<label js-showpassword>
    <input type="checkbox"> Show password
</label>
```

<div class="well blog-well">
    <input type="password" placeholder="Enter a password">
    <span class="label label-success" js-passwordstrength></span>
    <div js-showpassword>
        <label>
            <input type="checkbox"> Show password
        </label>
    </div>
</div>

## Further reading

* [The Ultimate UX Design of: the Sign-Up Form](http://designmodo.com/ux-sign-up-form/)
* [Good Users and Bad Passwords](http://www.sitepoint.com/good-users-bad-password-ux/)
* [zxcvbn: realistic password strenth estimation](https://blogs.dropbox.com/tech/2012/04/zxcvbn-realistic-password-strength-estimation/)
