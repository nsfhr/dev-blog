---
title: Coders do it standing up
tags:
    - office
authors:
    - rickmb
---

You've probably read at least one the many articles about [how sitting behind a desk all day is bad for your health](http://www.washingtonpost.com/national/health-science/too-much-sitting-may-have-some-serious-health-effects--even-if-you-exercise/2015/01/26/d0345a4a-a250-11e4-b146-577832eafcb4_story.html). It's even been referred to as the "sitting disease", and apparently can increase you chance of dying. Also, if you follow developer related news, you may have read some of the stories about developers experimenting with standing desks.

So when the time came to get some new desks, we really wanted them to be adjustable sitting/standing desks, so that we could try this out for ourselves, yet still be able to fall back on good old fashioned sitting. However, up to now most of these desks were prohibitively expensive, and none of us could be sure that we would actually be using them after the novelty wore of.

Lucky for us, Ikea released the new ["Bekant" sit/stand desk](http://www.ikea.com/nl/nl/catalog/products/S19022525/#/S69022537) a few months back <iframe src="http://www.youtube.com/embed/Yyl2NvKIK7M?rel=1&amp;autoplay=0" width="560" height="340" frameborder="0" style="display:block; margin: 0 auto"></iframe>

For a very affordable 519 euros (which is cheaper than many regular non-Ikea office desks), we could have our very own electronic standing desks!

Of course, given that it's Ikea, some assembly is required, and it doesn't come with an automated build process.
<img src="/images/desk_build.jpg" width="100%">

Finished desks:
<img src="/images/desk_done.jpg" width="100%">
Although not very large, the desks are big enough to support two 27" monitors, a laptop and a medium sized houseplant. Operating the desks is suprisingly quiet, quick and smooth. The only downside we've discovered so far is that there are no height presets, so if you regularly want to switch between standing and sitting, you'll have to improvise.

So, how do these desks work for us? It's a bit too early to tell, really. We'll get back to that in a few months time.
